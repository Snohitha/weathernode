const http = require("http") 

// code like the cool kids
// anonymous functions, abbreviating request and response 
const server = http.createServer((req, res) => {
  res.writeHead(200, { 'Content-Type': 'text/html' })
  res.write("<!DOCTYPE html>")
  res.write("<html><head><title>W05</title></head>")
  res.write("<body><h1>Hello World!!</h1></body></html>")
  res.end()
})
server.listen(8081)

console.log('Server running at http://127.0.0.1:8081/')
// How do we see our app?  What will appear?